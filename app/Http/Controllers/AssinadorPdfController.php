<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AssinadorPdfController extends Controller
{
    // Token de autenticação gerado no BRy Cloud
    const access_token = "tokenDeAutenticação";

    // Endereço de acesso ao HUB-Signer
    // Endereço de produção: https://hub2.bry.com.br
    // Endereço de homologação: https://hub2.hom.bry.com.br
    const uri_hub = "";


    public function assinarKMS(Request $request)
    {
        // Verifica se o documento enviado na requisição é válido.
        if ($request->hasFile('documento') && $request->file('documento')->isValid()) {
            $upload = $request->documento->storeAs('documentos', 'documentoParaAssinatura.pdf');
        } else {
            return response()->json(['message' => 'Arquivo enviado inválido'], 400);
        }

        // Recupera o tipo da credencial
        $kms_type = $request->kms_type;

        // Atualiza as informações do kms_data
        $kms_data = '"kms_data" : {';
        $kms_credencial = $request->valor_credencial;
        if (strcasecmp($kms_type, 'BRYKMS') == 0) {
            // Caso seja BRyKMS
            // Adiciona Credencial de acesso (PIN ou TOKEN)
            if ($request->kms_type_credential && strcasecmp($request->kms_type_credential, 'TOKEN') == 0) {
                $kms_data = $kms_data . '"token" : "' . $kms_credencial . '"';
            } else {
                $kms_data = $kms_data . '"pin" : "' . $kms_credencial . '"';
            }
            // Se parâmetros de identificação foram enviados, os adiciona
            if ($request->uuid_cert) {
                $kms_data = $kms_data . ',"uuid_cert" : "' . $request->uuid_cert . '"';
            } else if ($request->user) {
                $kms_data = $kms_data . ',"user" : "' . $request->user . '"';
            }
        } else if (strcasecmp($kms_type, 'GOVBR') == 0) {
            // Caso seja GOVBR
            // Adiciona token de acesso
            $kms_data = $kms_data . '"token" : "' . $kms_credencial . '"';
        } else if (strcasecmp($kms_type, 'DINAMO') == 0) {
            // Caso seja DINAMO
            // Adiciona Credencial de acesso (Pin, OTP ou Token)
            if ($request->kms_type_credential && strcasecmp($request->kms_type_credential, 'TOKEN') == 0) {
                $kms_data = $kms_data . '"token" : "' . $kms_credencial . '"';
            } else if ($request->kms_type_credential && strcasecmp($request->kms_type_credential, 'OTP') == 0){
                $kms_data = $kms_data . '"otp" : "' . $kms_credencial . '"';
            } else {
                $kms_data = $kms_data . '"pin" : "' . $kms_credencial . '"';
            }

            //Se usuário for enviado adiciona. Utilizado somente junto a PIN
            if ($request->user) {
                $kms_data = $kms_data . ',"user" : "' . $request->user . '"';
            }
            
            // Adiciona parâmetros para identificação de certificado e chave privada
            $kms_data = $kms_data . ',"uuid_cert" : "' . $request->uuid_cert . '"';
            $kms_data = $kms_data . ',"uuid_pkey" : "' . $request->uuid_pkey . '"';
        } else {
            return response(array('message' => 'KMS_TYPE Invalido', 400));
        }
        $kms_data = $kms_data . '},';

        // Verifica se a imagem enviada na requsição é valida.
        if ($request->incluirIMG === 'true') {
            if ($request->hasFile('imagem') && $request->file('imagem')->isValid()) {
                $upload = $request->imagem->storeAs('imagem', 'imagemAssinatura.png');
            } else {
                return response()->json(['message' => 'Imagem enviada inválida'], 400);
            }
        }        

        $dados_assinatura = 
        '{
            ' . $kms_data . '
            "algoritmoHash" : "'.$request->algoritmoHash.'",
            "perfil" : "' . $request->perfil . '"
        }';

        $data = array(
            'dados_assinatura' => $dados_assinatura,
            'documento' => new \CURLFILE(storage_path() . '/app/documentos/documentoParaAssinatura.pdf')
        );

        if($request->assinaturaVisivel === 'true') {
            $data['configuracao_imagem'] = '{
                "altura" : "' . $request->altura . '",
                "largura" : "' . $request->largura . '",
                "coordenadaX" : "' . $request->coordenadaX . '",
                "coordenadaY" : "' . $request->coordenadaY . '",
                "posicao" : "' . $request->posicao . '",
                "pagina" : "' . $request->pagina . '"
            }';

            $data['configuracao_texto'] = '{
                "texto" : "' . $request->texto . '",
                "incluirCN" : "' . $request->incluirCN . '",
                "incluirCPF" : "' . $request->incluirCPF . '",
                "incluirEmail" : "' . $request->incluirEmail . '"
            }';
            // Configurações da imagem da assinatura
            if ($request->incluirIMG === 'true') {
                $data['imagem'] = new \CURLFILE(storage_path() . '/app/imagem/imagemAssinatura.png');
            };
        }

        // Cria o CURL que será enviada à API de Assinatura.
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => self::uri_hub . "/fw/v1/pdf/kms/lote/assinaturas",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(
                'Authorization: ' . self::access_token,
                'kms_type: ' . $kms_type,
            ),
        ));

        $response = curl_exec($curl);
        $info = curl_getinfo($curl);
        curl_close($curl);

        $status_code = $info['http_code'];
        $objResponse = json_decode($response);
        if ($status_code != 200) {

            return response(array('message' => $objResponse->message), $status_code);
        }

        // Retorno o link do documento assinado para ser baixado no front-end
        return $objResponse->documentos[0]->links[0]->href;
    }
}
