# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2020-07-20

### Added

- Example of PDF signature. 

[1.0.0]: https://gitlab.com/brytecnologia-team/integracao/api-assinatura/php/geracao-de-assinatura-pdf-com-certificado-na-nuvem-kms/back-end/-/tags/1.0.0

## [1.1.0] - 2021-05-18

### Added

- Corrections on the code for the example project, to use new correct KMS credentials and some minor adjustments

[1.1.0]: https://gitlab.com/brytecnologia-team/integracao/api-assinatura/php/geracao-de-assinatura-pdf-com-certificado-na-nuvem-kms/back-end/-/tags/1.1.0
